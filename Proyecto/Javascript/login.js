/**
 * Login 
 *
*/
function login(){
    const address = document.getElementById('correo').value;
    const pass = document.getElementById('contraseña').value;
    // read users from the database
    const users = JSON.parse(localStorage.getItem('users'));
  
    // find the user with address and password
    const userFound = users.find((user) => {
    if(user.address3 == address && user.password == pass){
      return user;
      }
    });
  
    if(userFound) {
      alert(`Datos correctos, Bienvenido/a`);
      cleanFieldsLogin();
      // save username
      sessionStorage.setItem('usuario',userFound.name);
      window.open("Dashboard.html");
    } else {
      alert(`Datos incorrectos, Verifiqué sus datos`);
      cleanFieldsLogin();
    } 
  }
  /**
   * Clear the login fields
   *
  */
function cleanFieldsLogin(){
  document.getElementById('correo').value = "";
  document.getElementById('contraseña').value = ""; 
}