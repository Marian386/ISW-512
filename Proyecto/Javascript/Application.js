"use strict";

const USERSKEY = "users";
const NEWPRODUCTSKEY = "products";
const PRODUCTSKEY = "productsCambalaches";
/**
 *  Inserts a new user array in localstorage
 *
 */
function insertUsers() {
  const nom = document.getElementById("nombre").value;
  const lastname = document.getElementById('apellidos').value;
  const address1 = document.getElementById('correo1').value;
  const address2 = document.getElementById('correo2').value;
  const Country = document.getElementById('paises');
  const selectedCountry = Country.options[Country.selectedIndex].text; 
  const City = document.getElementById('ciudad').value;
  const address3 = document.getElementById('correoE').value;
  const password = document.getElementById('contraseña').value;
  let currentKey = localStorage.getItem("usersLastInsertedId");

  if (!currentKey) {
    localStorage.setItem("usersLastInsertedId", 1);
    currentKey = 1;
  } else {
    currentKey = parseInt(currentKey) + 1;
    localStorage.setItem("usersLastInsertedId", currentKey);
  }

  // create the user object
  const user = {
    id: currentKey,
    nom,
    lastname,
    address1,
    address2,
    selectedCountry,
    City,
    address3,
    password
  };

  // add it to the database
  let users = JSON.parse(localStorage.getItem(USERSKEY));
  if (users && users.length > 0) {
    users.push(user);
  } else {
    users = [];
    users.push(user);
  }
  localStorage.setItem(USERSKEY, JSON.stringify(users));

  // render the users
  debugger;
  renderTable("users", users);
}
/**
 *  Inserts a new product array in localstorage
 *
 */
function insertProducts() {
    const user = document.getElementById('name').value;
    const name = document.getElementById('nombre').value;
    const description = document.getElementById('descripcion').value;
    const direction = document.getElementById('direccion').value;
    const search = document.getElementById('busco').value;
    let currentKey = localStorage.getItem("usersLastInsertedId");

  if (!currentKey) {
    localStorage.setItem("usersLastInsertedId", 1);
    currentKey = 1;
  } else {
    currentKey = parseInt(currentKey) + 1;
    localStorage.setItem("usersLastInsertedId", currentKey);
  }

  // create the product object
  const product = {
    id: currentKey,
    user,
    name,
    description,
    direction,
    search
  };

   // add it to the database
   let products = JSON.parse(localStorage.getItem(NEWPRODUCTSKEY));
   if (products && products.length > 0) {
     products.push(product);
   } else {
     products = [];
     products.push(product);
   }
   localStorage.setItem(NEWPRODUCTSKEY, JSON.stringify(products));
 
   // render the products
   debugger;
   renderTable("products", products);
}