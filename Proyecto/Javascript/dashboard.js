/**
 * Show username
 *
*/
function username(){
    document.getElementById("name").value = sessionStorage.getItem('usuario');
}

/**
 * Show product list with a specific user
 *
*/
function showListOfProducts(){
    const products = JSON.parse(localStorage.getItem('products'));
    const table = document.getElementById('product_table');
    
    //read product from localstorage   
    let rows = "";
    products.forEach((product, index) => {
    let row = `<tr>`;
    row += `<td><img src="${product.direction}" alt="" width="200" height="150"></td>`;
    row += `<td><a href="DetalleProducto.html?id=${product.id}" class="link">${product.nameproduct}</a></td>`;
    row += `<td><button onclick="editProduct(${product.id})" class="boton-editar">Editar</button>
    <br><br>
    <button onclick="deleteProduct(${product.id})" class="boton-eliminar">Eliminar</button> </td>`
    rows += row + "</tr>";
    });
    table.innerHTML = rows;
    // generate the HTML table to show the product
  
}
/**
 * Edit a specific product
 *  @param {*} id
 *
*/
function editProduct(Id){
    // read all products from the database
    const product = JSON.parse(localStorage.getItem('products'));
  
    // find the product with Id
    const productFound = product.find((p) => {
      if(p.id == Id){
        return p;
      }
    });
    // render the information of the product in the edit form
    if(productFound) {
      sessionStorage.setItem('producto',productFound.id);
      window.open("EditarProducto.html");
    } else {
      alert(`Producto no encontrado`);
    }
}
/**
* Delete a specific product
*  @param {*} id
*
*/
function deleteProduct(Id){
    // read all products from the database
    const products = JSON.parse(localStorage.getItem('products'));
  
    // find the product with Id and remove it from the list
    const productEdited = [];
    products.forEach((p) => {
    if(p.id != Id){
      productEdited.push(p);
    }
    });
  
    // replace the existing array
    localStorage.setItem('products',JSON.stringify(productEdited));
    showListOfProducts();
}


username();
showListOfProducts();