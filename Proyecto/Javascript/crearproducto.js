/**
 * add new products
 *
*/
function addNewProducts() {
  const user = sessionStorage.getItem('usuario');
  const nameproduct = document.getElementById('nombre').value;
  const description = document.getElementById('descripcion').value;
  const direction = document.getElementById('direccion').value;
  const search = document.getElementById('busco').value;
  
  //insert to a database
  let productsDb = JSON.parse(localStorage.getItem('products'));
  if(!productsDb) {
    productsDb = [];
  }
  const p = {
    id: productsDb.length + 1,
    name: user,
    nameproduct: nameproduct,
    description: description,
    direction: direction,
    search: search
      
  }
  productsDb.push(p);
  localStorage.setItem('products', JSON.stringify(productsDb));
  
  cleanFieldsNewProduct();  
}
/**
* Clear the new products fields
*
*/
function cleanFieldsNewProduct(){
  document.getElementById('nombre').value = "";
  document.getElementById('descripcion').value = ""; 
  document.getElementById('direccion').value = "";
  document.getElementById('busco').value = ""; 
}