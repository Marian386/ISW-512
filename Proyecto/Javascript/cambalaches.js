/**
 * Show product in cambalaches
 *  
 *
*/
function showProductCambalaches(){
  const products = JSON.parse(localStorage.getItem('productsCambalaches'));
  const table = document.getElementById('cambalaches_table');
  
  //read product from localstorage   
  let rows = "";
  products.forEach((product, index) => {
  let row = `<tr>`;
  row += `<td><img src="${product.direction}" alt="" width="200" height="150"></td>`;
  row += `<td><a class="titulo">${product.nameproduct}</a>
  <br>
  <a class="subtitulo">${product.username}</a>
  </td>`;
  rows += row + "</tr>";
  });
  table.innerHTML = rows;
  // generate the HTML table to show the products
}
