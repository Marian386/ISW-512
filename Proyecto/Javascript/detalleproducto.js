/**
 * Show specific product
 *  
 *
*/
function showSpecificProduct(){ 
    let url_string = window.location.href;
    let url = new URL(url_string);
    let newID = url.searchParams.get("id");
    
    // read all products from the database
    const product = JSON.parse(localStorage.getItem('products'));
    const table = document.getElementById('product_img');
  
    // find the product with Id
    const productFound = product.find((p) => {
      if(p.id == newID){
        return p;
      }
    });
    // render the information of the product in the edit form
    if(productFound) {
      document.getElementById('titulo').value = productFound.nameproduct;
      document.getElementById('usuario').value = productFound.name;
      document.getElementById('descripcion').value = productFound.description;
      document.getElementById('busco').value = productFound.search;
  
      let rows = "";
      let row = `<tr>`;
      row += `<td><img src="${productFound.direction}" alt="" class="img"  width="600" height="400"></td>`;
      rows += row + "</tr>";
      
      table.innerHTML = rows;
      sessionStorage.setItem('direccion',productFound.direction);
    } else {
      alert(`Producto no encontrado`);
    }
}
/**
 * Add products for show in cambalaches
 *  
 *
*/ 
function addProductCambalaches(){
    //read product data
    const nameproduct = document.getElementById('titulo').value;
    const username = document.getElementById('usuario').value;
    const description = document.getElementById('descripcion').value;
    const search = document.getElementById('busco').value;
    const direction = sessionStorage.getItem('direccion');
    //insert to a database
    let prodCambDb = JSON.parse(localStorage.getItem('productsCambalaches'));
    if(!prodCambDb) {
      prodCambDb = [];
    }
    const p = {
      id: prodCambDb.length + 1,
      nameproduct: nameproduct,
      username: username,
      description: description,
      direction: direction,
      search: search
    }
    prodCambDb.push(p);
    localStorage.setItem('productsCambalaches', JSON.stringify(prodCambDb));
    alert('Producto agregado a Cambalaches');
}
showSpecificProduct();