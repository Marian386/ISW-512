/**
 * Add users
 *
*/
function addUsers() {
    //read user data
    const name = document.getElementById('nombre').value;
    const lastname = document.getElementById('apellidos').value;
    const address1 = document.getElementById('correo1').value;
    const address2 = document.getElementById('correo2').value;
    const Country = document.getElementById('paises');
    const selectedCountry = Country.options[Country.selectedIndex].text; 
    const City = document.getElementById('ciudad').value;
    const address3 = document.getElementById('correoE').value;
    const password = document.getElementById('contraseña').value;
  
    //insert to a database
    let usersDb = JSON.parse(localStorage.getItem('users'));
    if(!usersDb) {
      usersDb = [];
    }
    const user = {
      id: usersDb.length + 1,
      name: name,
      lastname: lastname,
      address1: address1,
      address2: address2,
      Country: selectedCountry,
      City: City,
      address3: address3,
      password: password
    }
    usersDb.push(user);
    localStorage.setItem('users', JSON.stringify(usersDb));
  
    cleanFields();
}  
/**
* Clean user input fields
*
*/
function cleanFields(){
    document.getElementById('nombre').value = "";
    document.getElementById('apellidos').value = "";
    document.getElementById('correo1').value = "";
    document.getElementById('correo2').value = "";
    document.getElementById('ciudad').value = "";
    document.getElementById('correoE').value = "";
    document.getElementById('contraseña').value = "";
}