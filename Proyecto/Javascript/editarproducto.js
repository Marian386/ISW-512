/**
 * Show a specific product
 *  
 *
*/
function showProduct(){
    // read all products from the database
    const products = JSON.parse(localStorage.getItem('products'));
    const id = sessionStorage.getItem('producto');
  
    // find the product with Id
    const productFound = products.find((p) => {
    if(p.id == id){
      return p;
    }
    });
    
    // render the information of the product in the edit form
    if(productFound) {
      document.getElementById('nombre').value = productFound.nameproduct;
      document.getElementById('descripcion').value = productFound.description;
      document.getElementById('direccion').value = productFound.direction;
      document.getElementById('busco').value = productFound.search;
    }else{
      alert('No has editado');
    }
}
/**
 * Save edited products
 *  
 *
*/
function saveProduct(){
    // get the data from fields
    const products = JSON.parse(localStorage.getItem('products'));
    const id = sessionStorage.getItem('producto');
    const nameproduct = document.getElementById('nombre').value;
    const description = document.getElementById('descripcion').value;
    const direction = document.getElementById('direccion').value;
    const search = document.getElementById('busco').value;
  
    // find the product in the database and edit it
    const productsEdited = products.map((p) => {
      if(p.id == id){
        p.nameproduct = nameproduct;
        p.description = description;
        p.direction = direction;
        p.search = search;
      }
      return p;
    });
  
    // replace the existing array
    localStorage.setItem('products',JSON.stringify(productsEdited));
    cleanFieldsNewProduct();
}
/**
* Clear the new products fields
*
*/
function cleanFieldsNewProduct(){
  document.getElementById('nombre').value = "";
  document.getElementById('descripcion').value = ""; 
  document.getElementById('direccion').value = "";
  document.getElementById('busco').value = ""; 
}
showProduct();