function addBook() {
  //read the book title field
  const bookName = document.getElementById('title').value;

  console.log('El libro es:', bookName);
  //insert to a database
  let booksDb = JSON.parse(localStorage.getItem('books'));
  if(!booksDb) {
    booksDb = [];
  }
  booksDb.push(bookName);
  localStorage.setItem('books', JSON.stringify(booksDb));
  //reload the book list
  showListOfBooks();
}

function showListOfBooks(){
  const bookName = localStorage.getItem('books');

  // read books from localstorage
  // generate the HTML table to show the boook
}


function validateTitle() {
  //read the book title field
  const bookName = document.getElementById('title').value;
  if(bookName.length > 3) {
    document.getElementById('add-book-button').disabled = false;
  } else {
    document.getElementById('add-book-button').disabled = true;
  }
}

showListOfBooks();


function showListOfBooks(){ 
   // read books from localstorage 
   document.getElementById("books_table").innerHTML = ""; const bookName = localStorage.getItem('books'); 
  let array = []; 
  for (var i = 0; i < bookName.length; i++){ array = bookName.split('"'); } 
  // generate the HTML table to show the boook 
  for (var i = 0; i < array.length; i++){ if(array[i].length>1){ document.getElementById("books_table").insertRow(-1).innerHTML = array[i]; } } 
}


document.querySelector("save-book-button").addEventListener("click", () => {

  let book = document.getElementById("editTitle");
  let author = document.getElementById("editAuthor");

  let user = {
      name: book.value,
      lastName: author.value,
  };

  book.value = "";
  author.value = "";

  guardar(editbook);

});

const localStorageName = "data";

function guardar(editbook) {
  let editb = [];
  let bookInLocalStorage = localStorage.getItem(localStorageName);
  if (bookInLocalStorage !== null) {
    editb = JSON.parse(bookInLocalStorage);
  }
  editb.push(editbook);
  localStorage.setItem(localStorageName, JSON.stringify(editb));
}